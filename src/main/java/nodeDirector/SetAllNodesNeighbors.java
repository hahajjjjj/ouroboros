package nodeDirector;

import blockchain.RequestSender;
import blockchain.Settings;
import helpers.Util;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class SetAllNodesNeighbors {
    public static void main(String[] args) {
        System.out.println("hello world!");
        String url = Util.createUrl(Settings.masterNodeAddress, Settings.getAllNodeAddressesPath);
        ArrayList<String> activeNodes = new ArrayList<String>();

        HashMap<String, String> nodeMapping = new HashMap<String, String>();

        String nodeMappingPath = "/Users/LarsErik/Skole/tsinghua/fag/computationalEconomics/semesterProjectServer/gs-rest-service/complete/src/main/java/nodeDirector/nodeMapping.txt";
        try {
            Files.lines(Paths.get(nodeMappingPath)).forEach( line -> {
                String[] lineParts =  line.split("=");
                String nodeAddress = lineParts[0];
                String newNeighbors = lineParts[1];
                nodeMapping.put(nodeAddress, newNeighbors);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        HashMap requestParamRequest1 = new HashMap<String, String>();


        Iterator it = nodeMapping.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            HashMap requestParamRequest2 = new HashMap<String, String>();
            requestParamRequest2.put("nodes" ,pair.getValue());
            String urlAddress = Util.createUrl((String) pair.getKey(), Settings.setNeighborsPath);
            try {
                System.out.println("url = " + urlAddress);
                RequestSender.makeRequest(urlAddress, requestParamRequest2, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            it.remove(); // avoids a ConcurrentModificationException
        }

//
//        try {
////            JSONObject allNodesResponse = RequestSender.makeRequest(url, requestParamRequest1, true);
//            for (Object obj :allNodesResponse.getJSONArray("nodes")){
//                String ipAddress = (String) obj;
//                String newNeighbors = nodeMapping.get(ipAddress);
//                if (newNeighbors != null){
//                    HashMap requestParamRequest2 = new HashMap<String, String>();
//                    requestParamRequest2.put("nodes", newNeighbors);
//                    String urlAddress = Util.createUrl(ipAddress, Settings.setNeighborsPath);
//                    System.out.println("url = " + urlAddress);
//                    RequestSender.makeRequest(urlAddress, requestParamRequest2, false);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
