package blockchain;

/**
 * Created by LarsErik on 17/12/2018.
 */
public class Slot {
    private int index;
    private Block block;
    private SlotLeader slotLeader;

    public Slot(int index){
        this.index = index;
    }

    public void setSlotLeader(SlotLeader slotLeader){
        this.slotLeader = slotLeader;
    }

    public Block getBlock() { return block; }

    public void setBlock(Block block) {
        this.block = block;
    }

    public SlotLeader getSlotLeader() {
        return slotLeader;
    }
}
