package blockchain;

import org.apache.coyote.http2.Setting;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Node {
    private static final Node nodeInstance = new Node();
    private List<Blockchain> blockchains;
    private AddressBook addressBook;
    private TransactionQueue transactionQueue;
    private RelayedTransactionsRegister relayedTransactionsRegister;

    @Value("${spring.profiles.active}")
    public String activeProfile;

    public static Node getInstance( ) {
        return nodeInstance;
    }

    private Node() {
        Blockchain firstBlockchain = new Blockchain(0);
        this.blockchains = new ArrayList<Blockchain>();
        this.blockchains.add(firstBlockchain);
        addressBook = new AddressBook();
        transactionQueue = new TransactionQueue();
        relayedTransactionsRegister = new RelayedTransactionsRegister();
    }

    public Blockchain getBlockchain(int index) { return blockchains.get(index); }
    public List<Blockchain> getBlockchains() { return blockchains; }
    public void addBlockchain(Blockchain blockchain){ blockchains.add(blockchain);}

    public void setBlockchain(Blockchain blockchain, int index) {
        this.blockchains.set(index, blockchain);
    }

    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    public TransactionQueue getTransactionQueue() {
        return transactionQueue;
    }

    public void setTransactionQueue(TransactionQueue transactionQueue) {
        this.transactionQueue = transactionQueue;
    }

    public RelayedTransactionsRegister getRelayedTransactionsRegister() {
        return relayedTransactionsRegister;
    }

    public void setRelayedTransactionsRegister(RelayedTransactionsRegister relayedTransactionsRegister) {
        this.relayedTransactionsRegister = relayedTransactionsRegister;
    }
}
