package blockchain;

import com.google.gson.Gson;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class testFile {
    public static void main(String[] args) {
        JSONObject json = new JSONObject();
        json.put("index",1);
        json.put("id", "randomId");
        json.put("timestamp",3);
        json.put("hash", "hash123");
        json.put("previousHash", "hash321");
        json.put("data", "blabla");
        json.put("epochNumber", 3);
        json.put("nonce", 7);
        json.put("SSCPayload", "paylShow Log in Finderoooad");

        List<Transaction> trasactionList = new ArrayList<Transaction>();
        trasactionList.add(new Transaction("","","", ""));
        json.put("transactions", trasactionList);

        System.out.print("string = " + json.toString());



        Gson gson = new Gson();
        Block theBlock = gson.fromJson(json.toString(),Block.class);

        String blockString =  gson.toJson(theBlock);
        System.out.println("---");
        System.out.println(blockString);
        Block theBlock2 = gson.fromJson(blockString,Block.class);


//        System.out.print(theBlock.getData());
//        System.out.print(theBlock);
    }
}
