package blockchain;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class RequestSender {

    public static JSONObject makeRequest(String url, HashMap<String, String> dict, Boolean waitForResponse) throws Exception {
//        System.out.println("Making request with url = " + url + " and param = " + dict.toString());
        HttpURLConnection con = null;
        System.out.println("making request to url " + url);
        try {
            URL obj = new URL(url);
            con = (HttpURLConnection) obj.openConnection();
            // connection to backend is open

            StringBuilder postData = new StringBuilder();
            for (HashMap.Entry<String, String> param : dict.entrySet()) { //gir: key1=value1&key1=value2&key3=value3...
                if (postData.length() != 0) {
                    postData.append('&');
                }
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            con.setRequestMethod("POST");

            con.setDoOutput(true);
            // send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.write(postDataBytes);
            wr.flush();
            wr.close();
            if (waitForResponse){
                // get response
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return new JSONObject(response.toString());
            }
            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
        return new JSONObject();
    }
}
