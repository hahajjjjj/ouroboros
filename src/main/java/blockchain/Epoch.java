package blockchain;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by LarsErik on 17/12/2018.
 */
public class Epoch {
    private int epochNumber;
    private ArrayList<Slot> slots = new ArrayList<Slot>();

    public Epoch(int epochNumber){
        this.epochNumber = epochNumber;
        for (int i = 0; i < Settings.epochLength; i++){
            Slot newSlot = new Slot(i);
            slots.add(newSlot);
        }
    }

    public ArrayList<Slot> getSlots(){
        return slots;
    }

    public Slot getSlot(int index){
        return slots.get(index);
    }

    public int getAviableSlotNumber(){
        for (int i = slots.size() - 1 ; i >= 0; i--){
            if (slots.get(i).getBlock() != null){
                return i + 1;
            }
        }
        return 0;
    }

    public int getEpochNumber() {
        return epochNumber;
    }
}
