package blockchain;

/**
 * Created by LarsErik on 17/12/2018.
 */
public class SlotLeader {

    private String address;

    public SlotLeader(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
