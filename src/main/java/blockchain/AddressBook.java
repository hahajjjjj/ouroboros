package blockchain;

import java.util.*;

public class AddressBook {
    private Set<String> registredAddresses = new LinkedHashSet<String>();

    public Set<String> getRegistredAddresses() {
        return registredAddresses;
    }
    public void addAddress(String address){
        if (!address.equals(Settings.currentNodeAddress)){
            System.out.println("Registrering address " + address);
            this.registredAddresses.add(address);
        }
        else{
            System.out.println("Trying to add address which equals currentNode.");
        }
    }

    public void setRegistredAddresses(List<String> addresses){
        this.registredAddresses = new HashSet<String>(addresses);
    }
}