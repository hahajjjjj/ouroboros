package blockchain;

import helpers.Util;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Ouroboros {
    private int currentEpoch = 0;
    private int currentSlot = 0;
    private long lastSlotUpdate;


    public int getCurrentEpoch() {
        return currentEpoch;
    }

    public void setCurrentEpoch(int currentEpoch) {
        this.currentEpoch = currentEpoch;
    }

    public int getCurrentSlot() {
        return currentSlot;
    }

    public void setCurrentSlot(int currentSlot) {
        this.currentSlot = currentSlot;
    }

    public List<SlotLeader> startNewEpoch(int epochNumber){
        String url = Util.createUrl(Settings.masterNodeAddress, Settings.getAllNodeAddressesPath);
        ArrayList<String> activeNodes = new ArrayList<String>();
        HashMap requestParam = new HashMap<String, String>();
        try {
            JSONObject allNodesResponse = RequestSender.makeRequest(url, requestParam, true);
            for (Object obj :allNodesResponse.getJSONArray("nodes")){
                String ipAddress = (String) obj;
                activeNodes.add(ipAddress);
                sendNewEpochToNode(epochNumber, ipAddress);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Random rand = new Random();

        List<SlotLeader> slotLeaders = new ArrayList<SlotLeader>();
        for (int i = 0; i < Settings.epochLength; i++){
            String randomElement = activeNodes.get(rand.nextInt(activeNodes.size()));
            slotLeaders.add(new SlotLeader(randomElement));
        }
        return slotLeaders;
    }

    public void tellNodeToCreateBlock(String address, int epoch, int slotNumber){
        HashMap requestParam = new HashMap<String, String>();
        requestParam.put("epoch", epoch);
        requestParam.put("slotNumber", slotNumber);
        System.out.println("tell node to create block!");
        String url = Util.createUrl(address, Settings.generateBlockPath);
        try {
            RequestSender.makeRequest(url, requestParam, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNewEpochToNode(int epochNumber, String address){
        HashMap requestParam = new HashMap<String, String>();
        String url = Util.createUrl(address, Settings.startNewEpochPath);
        requestParam.put("epoch", epochNumber);
        try {
            System.out.println("Sending newEpoch to address: " + url);
            System.out.println(url);
            RequestSender.makeRequest(url, requestParam, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getLastSlotUpdate() {
        return lastSlotUpdate;
    }

    public void setLastSlotUpdate(long lastSlotUpdate) {
        this.lastSlotUpdate = lastSlotUpdate;
    }
}
