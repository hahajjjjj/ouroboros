package blockchain;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by LarsErik on 17/12/2018.
 */
public class Block {

    private int slotNumber;
    private String id;
    private String timestamp;
    private String hash;
    private String previousHash;
    private String data;
    private int epochNumber;
    private int nonce;
    private String SSCPayload;
    private int blockchain;
    private List<Transaction> transactions = new ArrayList<Transaction>();

    public Block(String id, int blockchain, int epochNumber, int slotNumber, String timestamp, String previousHash, String data,String SSCPayload) {
        this.slotNumber = slotNumber;
        this.id = id ;
        this.timestamp = timestamp;
        this.previousHash = previousHash;
        this.data = data;
        this.SSCPayload = SSCPayload;
        this.epochNumber = epochNumber;
        this.blockchain = blockchain;
        nonce = 0;
    }

    public int getSlotNumber() {
        return slotNumber;
    }

    public String getSSCPayload(){return this.SSCPayload;}

    public String getTimestamp() { return timestamp; }

    public String getHash() {
        return hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public String getData() {
        return data;
    }

    public String str() {
        return String.valueOf(slotNumber) + timestamp + previousHash + data + nonce;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public int getEpochNumber() {
        return epochNumber;
    }

    public String getId() {
        return id;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getBlockchain() {
        return blockchain;
    }

    public void setBlockchain(int blockchain) {
        this.blockchain = blockchain;
    }
    @Override
    public String toString(){
        return "B(id:"+id+" slot:"+slotNumber+" transLen: "+transactions.size() + ")";
    }
}