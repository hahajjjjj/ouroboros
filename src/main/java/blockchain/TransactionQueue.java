package blockchain;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LarsErik on 17/12/2018.
 */
public class TransactionQueue {
    private ArrayList<Transaction> transactions = new ArrayList<Transaction>();

    public boolean containsTransaction(Transaction trans){
        return this.transactions.contains(trans);
    }

    public void setTransactions(ArrayList<Transaction> transactions){
        this.transactions = transactions;
    }

    public ArrayList<Transaction> getTransactions(){
        return transactions;
    }

    public void removeTransactions(List<Transaction> inputTransactions){
        this.transactions.removeAll(inputTransactions);
    }

    public boolean addTransaction(Transaction trans){
        if (!this.containsTransaction(trans)){
            transactions.add(trans);
            return true;
        }
        return false;
    }
}