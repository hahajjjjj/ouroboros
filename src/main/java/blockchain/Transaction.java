package blockchain;

/**
 * Created by LarsErik on 17/12/2018.
 */
public class Transaction {
    private String addressFrom;
    private String addressTo;
    private String payload;
    private String id;

    public Transaction(String id, String addressFrom, String addressTo, String payload){
        this.addressFrom = addressFrom;
        this.addressTo = addressTo;
        this.payload = payload;
        this.id = id;
    }

    public String getAddressFrom(){return this.addressFrom;}
    public String getAddressTo(){return this.addressTo;}
    public String getPayload() {return payload;}

    @Override
    public boolean equals(Object v) {
        boolean retVal = false;

        if (v instanceof Transaction) {
            Transaction ptr = (Transaction) v;
            retVal = (ptr.getId().equals(this.getId()));
        }
        return retVal;
    }

    @Override
    public String toString() {
            return "id = " + this.getId() + ", adrFrom = " + this.getAddressFrom() + " adrTo = " + this.getAddressTo()
                + " and payload = " + this.getPayload();
    }

    public String getId() {
        return id;
    }

}