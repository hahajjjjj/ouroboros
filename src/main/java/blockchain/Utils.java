package blockchain;

/**
 * Created by LarsErik on 17/12/2018.
 */

public class Utils {
    public static String zeros(int size){
        String returnString = "";
        for (int x = 0; x < size; x++){
            returnString += "0";
        }

        return returnString;
    }
}
