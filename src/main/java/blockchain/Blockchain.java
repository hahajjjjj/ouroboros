package blockchain;

import java.util.List;

/**
 * Created by LarsErik on 17/12/2018.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Blockchain {
    private int id;
    private List<Epoch> epochs;

    public Blockchain(int id) {
        epochs = new ArrayList<Epoch>();
        Epoch firstEpoch = new Epoch(0);
        epochs.add(firstEpoch);
        this.id = id;
    }

    public void addEpoch(int epochNumber){
        int loopLimit = epochNumber-latestEpoch().getEpochNumber();
        for (int i = 0; i < loopLimit; i++){
            int newEpochNumber = latestEpoch().getEpochNumber() + 1;
            Epoch newEpoch = new Epoch(latestEpoch().getEpochNumber() + 1);
            System.out.println("adding epoch with number " + String.valueOf(newEpochNumber));
            epochs.add(newEpoch);
        }
    }

    public Epoch latestEpoch() {
        return epochs.get(epochs.size() - 1);
    }

    public Boolean addBlock(Block block){
        Epoch epoch = getEpoch(block.getEpochNumber());
        Slot relevantSlot = epoch.getSlot(block.getSlotNumber());
        if (relevantSlot.getBlock() == null){
            relevantSlot.setBlock(block);
            return true;
        }
        return false;
    }

    public Epoch getEpoch(int index){
        if (index >= epochs.size()){
            throw new IllegalArgumentException("Index out of bounds");
        }
        return epochs.get(index);
    }
    @Override
    public String toString(){
        return "BC("+this.id+"):"+ epochs.get(0).getSlots().stream().filter(a -> a.getBlock() != null).map(a-> a.getBlock().toString()).collect(Collectors.joining("<->"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
