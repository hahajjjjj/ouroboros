package blockchain;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RelayedTransactionsRegister {
    private Map<String, Date> transactions = new HashMap<String, Date>();

    public Map<String, Date> getTransactions() {
        return transactions;
    }

    public void addTransaction(String address, String id) {
//        Pair<String, String> transactionPair = new Pair(address, id);
        if(!containsRelayedMessage(address, id)){
            Date currentDate = new Date();
            String transactionKey = address+id;
//            System.out.println("saving transaction with key: " + transactionKey);
            transactions.put(transactionKey, currentDate);
        }
    }

    public boolean containsRelayedMessage(String address, String id){
        String transactionKey = address+id;
        return transactions.containsKey(transactionKey);
    }
}