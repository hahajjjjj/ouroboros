package blockchain;


import helpers.Util;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class OuroborosRunner {
    public static void main(String[] args) {
        Ouroboros ouroborosClient = new Ouroboros();
        List<SlotLeader> slotLeaders = ouroborosClient.startNewEpoch(0);
        ouroborosClient.setLastSlotUpdate(System.currentTimeMillis());
        while (true){
            try {
                Thread.sleep(Settings.mainThreadSleepInterval);
                long intervalSinceLastUpdate = System.currentTimeMillis() - ouroborosClient.getLastSlotUpdate();

                if(intervalSinceLastUpdate > Settings.milisecondsBetweenBlocks){
                    int currentSlot = ouroborosClient.getCurrentSlot();
                    int currentEpoch = ouroborosClient.getCurrentEpoch();
                    SlotLeader leader = slotLeaders.get(currentSlot);
                    ouroborosClient.tellNodeToCreateBlock(leader.getAddress(), currentEpoch, currentSlot);

                    ouroborosClient.setLastSlotUpdate(System.currentTimeMillis());
                    ouroborosClient.setCurrentSlot(currentSlot + 1);

                    if (ouroborosClient.getCurrentSlot() >= Settings.epochLength){
                        ouroborosClient.setCurrentEpoch(currentEpoch + 1);
                        ouroborosClient.setCurrentSlot(0);
                        slotLeaders = ouroborosClient.startNewEpoch(currentEpoch+1);
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}