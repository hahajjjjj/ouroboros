package blockchain;

/**
 * Created by LarsErik on 17/12/2018.
 */
public class Settings {
    public static boolean isAdmin = false;

    public static int epochLength = 1000;
    public static int transactionLimitPerBlock = 100;
    public static int mainThreadSleepInterval = 1000;
    public static int milisecondsBetweenBlocks = 2000;

    public static String masterNodeAddress = "http://13.238.154.124:8080";
    public static String currentNodeAddress = "http://localhost:8080";

    public static final String registerSlaveServerPath = "registerSlaveServer";
    public static final String postTransactionPath = "postTransaction";
    public static final String postBlockPath = "postBlock";
    public static final String generateBlockPath = "generateBlock";
    public static final String getAllNodeAddressesPath = "getAllNodeAddresses";
    public static final String startNewEpochPath = "startNewEpoch";
    public static final String registerNodePath = "registerNode";
    public static final String setNeighborsPath = "setNeighbors";
    public static final String generateBlockchainPath = "generateBlockchain";
}