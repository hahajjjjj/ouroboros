/**
 * Created by LarsErik on 17/12/2018.
 */
package server;

import blockchain.*;
import com.google.gson.Gson;
import com.sun.org.apache.xalan.internal.xsltc.dom.AdaptiveResultTreeImpl;
import helpers.Util;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.crypto.dsig.TransformService;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static java.lang.System.currentTimeMillis;


@RestController
public class InboundTrafficController {
    @RequestMapping(value = "/" + Settings.postTransactionPath ,method = RequestMethod.POST)
    public String postTransaction(@RequestParam(value="from") String from,
                                 @RequestParam(value="to") String to,
                                 @RequestParam(value="payload") String payload,
                                 @RequestParam(value="id") String id){

        Transaction transaction = new Transaction(id, from, to, payload);
        Node nodeInstance = Node.getInstance();

        if (!nodeInstance.getTransactionQueue().containsTransaction(transaction)){
            nodeInstance.getTransactionQueue().addTransaction(transaction);
        }
        HashMap requestParam = new HashMap<String, String>();
        requestParam.put("from", from);
        requestParam.put("to", to);
        requestParam.put("pyaload", payload);
        requestParam.put("id", id);
        String serverUrl = "/" + Settings.postTransactionPath;

        relayTransactionToAllNodes(id, serverUrl, requestParam);

        JSONObject responseArray = new JSONObject();
        responseArray.put("status","success");
        return responseArray.toString();
    }

    @RequestMapping(value = "/" + Settings.postBlockPath ,method = RequestMethod.POST)
    public String postBlock(@RequestParam(value="blockString") String blockString
    ){
        Gson gson = new Gson();
        Block block = gson.fromJson(blockString,Block.class);
        block.setHash(block.getHash() + "3");
        Node nodeInstance = Node.getInstance();
        System.out.println("Got postBlock request, the timestamp is = " + block.getTimestamp());
        System.out.println("Current timestamp = " + System.currentTimeMillis());
        long diff = System.currentTimeMillis() - Long.parseLong(block.getTimestamp());
        System.out.println("Diff = " + String.valueOf(diff));
        System.out.println("Block hash = " + block.getHash());
        try{
            nodeInstance.getBlockchain(block.getBlockchain()).addBlock(block);
            nodeInstance.getTransactionQueue().removeTransactions(block.getTransactions());
            String serverUrl = "/" + Settings.postBlockPath;

            HashMap requestParam = new HashMap<String, String>();
            requestParam.put("blockString", gson.toJson(block));
            relayTransactionToAllNodes(block.getId(), serverUrl, requestParam);
        }
        catch (IllegalArgumentException e){
            System.out.print("Error: the epochNumber was out of bounds");
        }
        System.out.println(nodeInstance.getBlockchain(block.getBlockchain()));
        return "{status:success}";
    }

    @RequestMapping(value = "/" + Settings.startNewEpochPath, method = RequestMethod.POST)
    public String startNewEpoch(@RequestParam(value="epoch") int epochNumber){
        Node nodeInstance = Node.getInstance();
        for (int i = 0; i < nodeInstance.getBlockchains().size(); i++){
            nodeInstance.getBlockchain(i).addEpoch(epochNumber);
        }
        return "{status:success}";
    }

    @RequestMapping(value = "/" + Settings.registerNodePath, method = RequestMethod.POST)
    public String registerNode(@RequestParam(value="ipAddress") String ipAddress){
        Node nodeInstance = Node.getInstance();
        nodeInstance.getAddressBook().addAddress(ipAddress);
        return "{status:success}";
    }

    @RequestMapping(value = "/" + Settings.setNeighborsPath, method = RequestMethod.POST)
    public String setNeighbor(@RequestParam(value="nodes") String nodes){

        String[] newNodes = nodes.split(",");
        Node nodeInstance = Node.getInstance();

        nodeInstance.getAddressBook().setRegistredAddresses(Arrays.asList(newNodes));
        return "{status:success}";
    }

    @RequestMapping(value = "/" + Settings.registerSlaveServerPath, method = RequestMethod.POST)
    public String registerSlaveServer(@RequestParam(value="ipAddress") String ipAddress, HttpServletRequest request){
        String requestIp = "http://" + request.getRemoteAddr() + ":8080";
        System.out.println("Got request from ip: " + requestIp);

        Node nodeInstance = Node.getInstance();
        AddressBook currentAddressbook = nodeInstance.getAddressBook();
        Set<String> allreadyRegistredAddressesSet = currentAddressbook.getRegistredAddresses();
        String[] allreadyRegistredAddresses = allreadyRegistredAddressesSet.toArray(new String[allreadyRegistredAddressesSet.size()]);
        currentAddressbook.addAddress(requestIp);
        nodeInstance.setAddressBook(currentAddressbook);

        JSONObject response = new JSONObject();
        response.put("nodes", allreadyRegistredAddresses);

        System.out.println("Register slave with ip = " + requestIp);

        return response.toString();
    }

    @RequestMapping(value = "/" + Settings.generateBlockPath, method = RequestMethod.POST)
    public String generateBlock(@RequestParam(value="epoch") int epoch,
                                @RequestParam(value="slotNumber") int slotNumber,
                                @RequestParam(required=false,value="messageSize", defaultValue = "0") int messageSize,
                                @RequestParam(required=false,value="transactionsPerBlock", defaultValue = "1000") int transactionsPerBlock){
        Node nodeInstance = Node.getInstance();
        Gson gson = new Gson();
        System.out.println("Request to generate block");
        for (int i = 0; i < nodeInstance.getBlockchains().size(); i++){
            String blockId = String.valueOf(i) + Long.toString(currentTimeMillis());
            String timestamp = String.valueOf(System.currentTimeMillis());

            ArrayList<Transaction> transactionsInBlock = new ArrayList<Transaction>();
            for (int t = 0; t < transactionsPerBlock; t++){
                String loopId = String.valueOf(i);
                if (i < 10){
                    loopId = "0" + loopId;
                }
                String payload = Util.createDataSize(messageSize);
                Transaction newTransaction = new Transaction(timestamp+i+t,"","",payload);
                transactionsInBlock.add(newTransaction);
            }

            Block newBlock = new Block(blockId, i, epoch, slotNumber, timestamp,  "", "", blockId);
            newBlock.setTransactions(transactionsInBlock);
            nodeInstance.getBlockchain(i).addBlock(newBlock);

            System.out.println(nodeInstance.getBlockchain(i));

            HashMap requestParam = new HashMap<String, String>();
            requestParam.put("blockString", gson.toJson(newBlock));
            String serverUrl = "/" + Settings.postBlockPath;

            if (messageSize != 0){
                try {
                    long messageLength = gson.toJson(newBlock).getBytes("UTF-8").length;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

            relayTransactionToAllNodes(newBlock.getId(), serverUrl , requestParam);
        }
        return "{status:success}";
    }

    @RequestMapping(value = "/" + Settings.getAllNodeAddressesPath, method = RequestMethod.POST)
    public String getAllNodeAddresses() {
        Node nodeInstance = Node.getInstance();
        Set<String> allreadyRegistredAddressesSet = nodeInstance.getAddressBook().getRegistredAddresses();
        String[] allreadyRegistredAddresses =
                allreadyRegistredAddressesSet.toArray(new String[allreadyRegistredAddressesSet.size()]);

        JSONObject response = new JSONObject();
        response.put("nodes", allreadyRegistredAddresses);
        System.out.println("hello!");
        return response.toString();
    }


    @RequestMapping(value = "/" + Settings.generateBlockchainPath, method = RequestMethod.POST)
    public String generateBlockchain(@RequestParam(value="index") int index){
        Node nodeInstance = Node.getInstance();
        int currentNumberOfBlockchains = nodeInstance.getBlockchains().size();
        int numberOfBlockchainsToCreate = index - nodeInstance.getBlockchains().size();
        for (int i = 0; i < numberOfBlockchainsToCreate; i++){
            Blockchain newBlockchain = new Blockchain( (i+currentNumberOfBlockchains));
            nodeInstance.addBlockchain(newBlockchain);
        }
        System.out.println("currentNumberOfBlockchains = " + nodeInstance.getBlockchains().size());
        return "{status:success}";
    }

    private void relayTransactionToAllNodes(String messageId, String serverUrl, HashMap<String, String> requestParam){
        Node nodeInstance = Node.getInstance();

        Set<String> addressSet = nodeInstance.getAddressBook().getRegistredAddresses();
        RelayedTransactionsRegister relayRegister = nodeInstance.getRelayedTransactionsRegister();

        for (String nodeAddress : addressSet.toArray(new String[addressSet.size()])) {
            String requestUrl = nodeAddress + serverUrl;
            System.out.println("Checking for nodeAddress: "+ nodeAddress + "message id = "+ messageId);
            if(!relayRegister.containsRelayedMessage(nodeAddress, messageId)) {
                try {
                    relayRegister.addTransaction(nodeAddress, messageId);
                    RequestSender.makeRequest(requestUrl, requestParam, true);
                } catch (Exception e) {
                    System.out.print("Error: sending request in relayTransactionToAllNodes");
                }

            }
        }
    }
}