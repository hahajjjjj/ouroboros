package server;

import blockchain.Node;
import blockchain.RequestSender;
import blockchain.Settings;
import com.google.gson.JsonArray;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.ip.udp.UnicastReceivingChannelAdapter;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@SpringBootApplication

public class Application {
//    @Bean
//    public IntegrationFlow processUniCastUdpMessage() {
//        return IntegrationFlows
//                .from(new UnicastReceivingChannelAdapter(11111))
//                .handle("UDPServer", "handleMessage")
//                .get();
//    }
//
//    @Service
//    public class UDPServer
//    {
//        public void handleMessage(Message message)
//        {
//            String data = new String((byte[]) message.getPayload());
//            System.out.print(data);
//        }
//    }

    @Value("${spring.profiles.active}")
    private static String activeProfile;

    public static void main(String[] args) {

        String systemEnvString = System.getenv("args");
        String[] SystemArgs = new String[0];
        String seperator = ":";
        if (args != null){
            SystemArgs = args;
            seperator = "=";
        }
        if (systemEnvString != null){
            SystemArgs = systemEnvString.split(",");
            seperator = ":";
        }


        String nodeAddress = "http://localhost:";

        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            nodeAddress = socket.getLocalAddress().getHostAddress();
            Settings.currentNodeAddress = nodeAddress + ":8080";
            System.out.println("CurrentNodeAddress = " + Settings.currentNodeAddress);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        for(String arg:SystemArgs) {
            String trimmedArgString = arg.replaceAll("--", "");
            String argumentName = trimmedArgString.split(seperator)[0];
            String argumentValue = trimmedArgString.split(seperator)[1];

            if (argumentName.equals("root") && argumentValue.equals("true")){
                Settings.isAdmin = true;
                System.out.println("Setting this node as root");
            }
            if (argumentName.equals("server.port")){
                Settings.currentNodeAddress = nodeAddress + ":" + argumentValue;
            }
        }

        HashMap requestParam = new HashMap<String, String>();
        Node nodeInstance = Node.getInstance();
        requestParam.put("ipAddress", Settings.currentNodeAddress);
        String postUrl = Settings.masterNodeAddress + "/" + Settings.registerSlaveServerPath;

        try {
            JSONObject neighborNodesResponse = RequestSender.makeRequest(postUrl, requestParam, true);
            for (Object obj :neighborNodesResponse.getJSONArray("nodes")){
                String ipAddress = (String) obj;
                nodeInstance.getAddressBook().addAddress(ipAddress);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SpringApplication.run(Application.class, args);
    }
}
